extends Node

signal new_image

var requestror:HTTPRequest = null
var requests = []
var current_request = null
var request_wait:bool = false
var json_data = null

var image_req_fields = ['name','type','timestamp','url','local']
var image_mimetypes = ['image/jpeg','image/png']

func push_request( type, url, data = null ):
	print( "request enqueued, type", type, ", url: ", url )
	requests.append( {'type': type, 'url': url, 'data': data } )

func push_image_request( idata:Dictionary ):
	for f in image_req_fields:
		if not f in idata:
			print( "Invalid image request" )
			return
	# adding complete local path
	idata.resource_path = 'user://' + str(idata.timestamp) + '.' + idata.local
	push_request( Image, idata.url, idata )
	print( 'requesting image: ' + idata.url )

func send_get( req ):
	var error = requestror.request(req)
	if error != OK:
		print("An error occurred in the HTTP request: " + req)

func validate_response( header, body ):
	if str(header).find( "application/json" ) > 0:
		var pj = parse_json(body.get_string_from_utf8())
		if typeof(pj) == TYPE_DICTIONARY:
			json_data = pj
			return true
		else:
			print("Json response MUST be a dictionary")
	else:
		print("response does not contains a valid json")
	json_data = null
	return false

func parse_objects():
	var version_maj = 	json_data.version[0]
	var version_min = 	json_data.version[1]
	var timestamp =		json_data.timestamp
	var obects =		json_data.objects
	for o in obects:
		if o.type == 'image':
			push_image_request( o )

func parse_image( headers, raw ):
	if current_request == null:
		return
	var valid:bool = false
	for mt in image_mimetypes:
		if str(headers).find( mt ) > 0:
			valid = true
			break
	if !valid:
		print( "response does not contains an image" )
		return
#	print( headers )
#	print( current_request.data )
	var f:File = File.new()
	if f.open( current_request.data.resource_path, File.WRITE ) != OK:
		print( "error in opening image: ", current_request.data.resource_path )
		return
	f.store_buffer( raw )
	f.close()
	print( "image received and saved in " + current_request.data.resource_path )
	emit_signal( "new_image", current_request.data.resource_path )

func _http_request_completed(result, response_code, headers, body):
	if current_request == null:
		print( "current_request must contains a dictionary describing the request" )
	#print( headers )
	match current_request.type:
		JSON:
			if validate_response( headers, body ):
				if 'objects' in json_data:
					parse_objects()
				else:
					print( 'no idea what this is: ', json_data )
		Image:
			parse_image( headers, body )

	current_request = null
	request_wait = false
	if requests.empty():
		print( "no more requests" )

func _ready():
	requestror = HTTPRequest.new()
	add_child(requestror)
	requestror.connect("request_completed", self, "_http_request_completed")
	push_request( JSON, "http://polymorph.cool/godot/resources.json" )

func _process(delta):
	if request_wait:
		return
	if requests.empty():
		return
	request_wait = true
	current_request = requests.pop_front()
	send_get( current_request.url )
