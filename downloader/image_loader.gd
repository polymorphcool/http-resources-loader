extends Node

export (NodePath) var http : NodePath = ""
export (NodePath) var target : NodePath = ""

var target_node : Node = null

func _load_image( np ):
	var i : Image = Image.new()
	if i.load( np ) != OK:
		print( "failed to load ", np )
	var tr : TextureRect = TextureRect.new()
	tr.texture = ImageTexture.new()
	tr.texture.create_from_image( i )
	tr.expand = true
	tr.rect_size = Vector2.ONE * 64
	tr.rect_min_size = Vector2.ONE * 64
	tr.stretch_mode = TextureRect.STRETCH_KEEP_ASPECT_CENTERED
	target_node.add_child( tr )
	var lb : Label = Label.new()
	lb.text = np
	target_node.add_child( lb )
	print( ">>>>> ", np )
	
func _ready():
	var downloader = get_node( http )
	if downloader == null or !downloader.has_signal( "new_image" ):
		print( "invalid path: ", http )
		return
	target_node = get_node( target )
	if target_node == null:
		print( "invalid target: ", target )
		return
	downloader.connect( "new_image", self, "_load_image" )
